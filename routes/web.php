<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//homepage
Route::get('/', 'PagesController@index');

//about page
Route::get('/about', 'PagesController@about');

//services page
Route::get('/services', 'PagesController@services');

Route::resource('posts', 'PostController');