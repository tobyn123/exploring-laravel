<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index(){

        $title = 'Welcome to lsapp';
        // return view('pages.index', compact('title'));
        return view('pages.index')->with('title', $title);

    }

    public function about(){
        $title = 'About Lsapp';
        return view('pages.about')->with('title', $title);
    }

    public function services(){
        
        $data = array(
            'title'=>'Services',
            'services'=> ['Web Design', 'Web Dev', 'Programming']
        );
        return view('pages.services')->with($data);
    }


}
