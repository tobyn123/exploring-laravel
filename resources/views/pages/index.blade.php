@extends('layouts.app')

@section('content')


<div class="jumbotron text-center">
        <h1>{{$title}}</h1>
        
        <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a></p>
</div>

@endsection